$.ajax({
              type:'get',
              dataType:'jsonp',
              url:'http://dna.yoka.com/weixin/WeixinService.asmx/GetTicket',
              callback:'callback',
              success:function(data){
                     wx.config({
                      debug: false,
                      appId: data.appID,
                      timestamp: data.timestamp,
                      nonceStr: data.noncestr,
                      signature: data.signature,
                      jsApiList: [
                        'onMenuShareTimeline',
                        'onMenuShareAppMessage',
                        'onMenuShareQQ',
                        'onMenuShareWeibo',
                        'previewImage'
                      ]
                  });
                },
                error:function(){}
          });  
      wx.ready(function () {
        wx.error(function(res){
            //console,log(res);
            // config信息验证失败会执行error函数，如签名过期导致验证失败，具体错误信息可以打开config的debug模式查看，也可以在返回的res参数中查看，对于SPA可以在这里更新签名。

        });

        wx.checkJsApi({
            jsApiList: ['onMenuShareTimeline','onMenuShareAppMessage','onMenuShareQQ','onMenuShareWeibo'], // 需要检测的JS接口列表，所有JS接口列表见附录2,
            success: function(res) {
                // 以键值对的形式返回，可用的api值true，不可用为false
                // 如：{"checkResult":{"chooseImage":true},"errMsg":"checkJsApi:ok"}
            }
        });

      wx.onMenuShareAppMessage(shareData);
      wx.onMenuShareTimeline(shareData);
      wx.onMenuShareQQ(shareData);
      wx.onMenuShareWeibo(shareData);
    });