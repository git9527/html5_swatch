//$ = Zepto;
var g_ship;
function Ship(ctx,pic){
	//gameMonitor.im.loadImage([pic]);
	this.width = 242;
	this.height = 120;
	this.left = gameMonitor.w/2 - this.width/2;
	this.top = gameMonitor.h - 2*this.height;
	this.player = gameMonitor.im.createImage(pic);

	this.paint = function(){
		ctx.drawImage(this.player, this.left, this.top, this.width, this.height);
	}

	this.setPosition = function(event){
		if(gameMonitor.isMobile()){
			var tarL = event.changedTouches[0].clientX;//event.touches[0].clientX;
			var tarT = event.changedTouches[0].clientY;//event.touches[0].clientY;
		}
		else{
			var tarL = event.offsetX;
			var tarT = event.offsetY;
		}
		this.left = tarL - this.width/2 - 32;
		this.top = tarT - this.height/2;
		if(this.left<0){
			this.left = 0;
		}
		if(this.left>$(window).width()-this.width){
			this.left =$(window).width()-this.width;
		}
		if(this.top<0){
			this.top = 0;
		}
		if(this.top>gameMonitor.h - this.height){
			this.top = gameMonitor.h - this.height;
		}
		//this.paint();
	}

	this.controll = function(){
		var _this = this;
		var stage = document.querySelector('#gamepanel');
		var currentX = this.left,
			currentY = this.top,
			move = false;
		stage.addEventListener(gameMonitor.eventType.start, function(event){
			if(gameMonitor.count!=gameMonitor.total){
				_this.setPosition(event);
				move = true;
			}
		},false);
		stage.addEventListener(gameMonitor.eventType.end, function(){
			move = false;
		},false)
		stage.addEventListener(gameMonitor.eventType.move, function(event){
			event.preventDefault();
			if(move&&gameMonitor.count!=gameMonitor.total){
				_this.setPosition(event);	
			}
			
		},false);
	}

	this.eat = function(foodlist){
		for(var i=foodlist.length-1; i>=0; i--){
			var f = foodlist[i];
			
			if(f&&gameMonitor.count<gameMonitor.total){
				var l1 = this.top+this.height/2 - (f.top+f.height/2);
				var l2 = this.left+this.width/2 - (f.left+f.width/2);
				var l3 = Math.sqrt(l1*l1 + l2*l2);
				if(l3<=this.height/2 + f.height/2){
					
					//var ctx =  document.getElementById('stage').getContext('2d');
					var pic = '';
					if(f.type == 0){
						pic += 'images/te.png';
					}else if(f.type == 1){
						pic += 'images/fe.png';
					}else if(f.type == 2){
						pic += 'images/oe.png';
					}else{
						pic += 'images/ce.png';
					}
					f.pic = gameMonitor.im.createImage(pic);
					
					//pic = gameMonitor.im.createImage(pic);
					//ctx.drawImage(pic, f.left, f.top, f.width, f.height);
					// var id = 'img'+i;
					// $('#gamepanel').append('<img id="'+id+'" src="'+pic+'" style="width:'+f.width+'px;height:'+f.height+'px;position:absolute;left:'+f.left+'px;top:'+f.top+'px;">');
					// $('#'+id).addClass('on');
					// setTimeout(function(){
					// 	$('#gamepanel img').hide();
					// },200);
					var id = f.id;
					setTimeout(function(){
						foodlist[id] = null;
					},200)
					gameMonitor.pos = i;
					//foodlist[f.id] = null;
					if(f.type!=gameMonitor.right && f.state){
						console.log(gameMonitor.count)
						++gameMonitor.count;
						//console.log(gameMonitor.count)
						$('#heart'+gameMonitor.count).hide();
						if(gameMonitor.count == gameMonitor.total){
							
							gameMonitor.stop();
							gameMonitor.updateShare();
							//game over 
							try{
								mzVirtualPV('#提交信息页', '抽奖分享');//add 提交信息页
							}catch(e){}
							$('#resultPanel').show();
							//gameMonitor.getScore();
							
						}
					}else{
						if(f.state) $('#score').text((++gameMonitor.score)*10 +"分");
					}
					f.state = 0;
				}
			}else{
				break;
			}
			
		}
	}
}

function Food(type, left, id){
	var r = Math.round(Math.random()*120);
	if(r<=100&&r>60){
		this.width = r;
		this.height = r;
	}else{
		this.width = 110;
		this.height = 110;
	}
	this.speedUpTime = 300;
	this.id = id;
	this.type = type;
	this.state = 1;
	this.left = left;
	this.top = -this.height;
	this.speed = gameMonitor.speed;//0.04 * Math.pow(1.2, Math.floor(gameMonitor.time/this.speedUpTime));
	this.loop = 0;
	var p = '';
	if(this.type == 0){
		p += 'images/t.png';
	}else if(this.type == 1){
		p += 'images/f.png';
	}else if(this.type == 2){
		p += 'images/o2.png';
	}else if(this.type == 3){
		p += 'images/c.png';
	}
	this.pic = gameMonitor.im.createImage(p);
}
Food.prototype.paint = function(ctx){
	ctx.drawImage(this.pic, this.left, this.top, this.width, this.height);
}
Food.prototype.move = function(ctx){
	/*if(gameMonitor.time % this.speedUpTime == 0){
		this.speed *= 1.2;
	}
	this.top += ++this.loop * this.speed;*/
	//this.speed += 0.1;
	if(this.state == 1){
		this.top += this.speed;
	}
	if(this.top>gameMonitor.h){
	 	gameMonitor.foodList[this.id] = null;
	}
	else{
		this.paint(ctx);
	}
}



function ImageMonitor(){
	var imgArray = [];
	return {
		createImage : function(src){
			return typeof imgArray[src] != 'undefined' ? imgArray[src] : (imgArray[src] = new Image(), imgArray[src].src = src, imgArray[src])
		},
		loadImage : function(arr, callback){
			for(var i=0,l=arr.length; i<l; i++){
				var img = arr[i];
				imgArray[img] = new Image();
				imgArray[img].onload = function(){
					if(i==l-1 && typeof callback=='function'){
						callback();
					}
				}
				imgArray[img].src = img
			}
		}
	}
}

var requestAnimationFrame = window.requestAnimationFrame
                                || window.webkitRequestAnimationFrame;
    var cancelAnimationFrame = window.cancelAnimationFrame 
    							|| window.webkitCancelAnimationFrame;

var gameMonitor = {
	total:5,
	count:0,
	intervalFlag:true,
	rate:400,
	w : $(window).width(),
	h : $(window).height(),
	bgWidth : $(window).width(),
	bgHeight : $(window).height(),
	time : 0,
	timmer : 0,
	bgSpeed : 2,
	bgloop : 0,
	score : 0,
	right:0,
	speed:6,
	interval:null,
	interval2:null,
	im : new ImageMonitor(),
	foodList : [],
	bgDistance : 0,//背景位置
	eventType : {
		start : 'touchstart',
		move : 'touchmove',
		end : 'touchend'
	},
	init : function(rig,pic){
		var _this = this;
		var canvas = document.getElementById('stage');
		var ctx = canvas.getContext('2d');
		_this.right = rig;
		
		/*_this.ship = new Ship(ctx,pic);
		_this.ship.paint();
  		_this.ship.controll();
  		gameMonitor.run(ctx);*/
		_this.initListener(ctx,pic);
	},
	initListener : function(ctx,pic){
		var _this = this;
		var body = $('#container');
		body.off(gameMonitor.eventType.start + ' ' +gameMonitor.eventType.move);

		body.on(gameMonitor.eventType.move, function(event){
			event.preventDefault();
		});
		body.on(gameMonitor.eventType.start, '#stage4', function(){
			try{
				mzBtnTrack('开始游戏',1);
				mzVirtualPV('#游戏页', '抽奖分享');//add 游戏界面
			}catch(e){}
			
			$("#stage4").hide();
			// _this.ship = new Ship(ctx,pic);
			// _this.ship.paint();
   //    		_this.ship.controll();
   			g_ship = new Ship(ctx,pic);
   			g_ship.paint();
   			g_ship.controll();
   			_this.timer = 1;
			gameMonitor.run(ctx);
		});



	},
	run : function(ctx){
		var _this = this;
		if(!_this.timer) return;
		if(_this.intervalFlag){
			_this.startInterval();
			_this.intervalFlag = false;
		}
		ctx.clearRect(0, 0, _this.bgWidth, _this.bgHeight);
		//_this.rollBg(ctx);

		
		//产生食材
		//_this.genorateFood();

		//绘制食材
		for(i=_this.foodList.length-1; i>=0; i--){
			var f = _this.foodList[i];
			if(f){
				//f.paint(ctx);
				f.move(ctx);
			}else{
				break;
			}
		}
		//ctx.drawImage(g_ship.player,g_ship.left,g_ship.top,g_ship.width,g_ship.height);
		//g_ship.paint();
		//绘制盘子
		g_ship.paint();
		g_ship.eat(_this.foodList);

		// requestAnimationFrame(function(){
		// 	_this.timmer = 1;
		// 	gameMonitor.run(ctx);
		// })
		_this.timmer = setTimeout(function(){
			gameMonitor.run(ctx);
		},  16);

	},
	stop : function(){
		var _this = this
		$('#stage').off(gameMonitor.eventType.start + ' ' +gameMonitor.eventType.move);
		clearInterval(_this.interval);
		clearInterval(_this.interval2);
		//_this.timmer = 0;
		setTimeout(function(){
			clearTimeout(_this.timmer);
		}, 0);
		
	},
	genorateFood : function(){
		var _this = this;
		var genRate = 50; //产生月饼的频率
		var random = Math.random();
		//if(random*genRate>genRate-1){
			var l = Math.random()*(_this.w - 116);
			var left =  l > 140 ? l : 140;
			//var type = Math.floor(left)%2 == 0 ? 0 : 1;
			var type = _this.generateRandom();
			var id = _this.foodList.length;
			var f = new Food(type, left, id);
			_this.foodList.push(f);
		//}
	},
	generateRandom:function(){
		var rand = Math.floor(Math.random() * 100);
		if(gameMonitor.right == 0){
			if(rand < 60){
				return 0;
			}else if(rand < 75){
				return 1;
			}else if(rand < 85){
				return 2;
			}else {
				return 3;
			}
		}else if(gameMonitor.right == 1){
			if(rand < 60){
				return 1;
			}else if(rand < 75){
				return 0;
			}else if(rand < 85){
				return 2;
			}else {
				return 3;
			}
		}else if(gameMonitor.right == 2){
			if(rand < 60){
				return 2;
			}else if(rand < 75){
				return 0;
			}else if(rand < 85){
				return 1;
			}else {
				return 3;
			}
		}else if(gameMonitor.right == 3){
			if(rand < 60){
				return 3;
			}else if(rand < 75){
				return 0;
			}else if(rand < 85){
				return 2;
			}else {
				return 1;
			}
		}
	},
	startInterval:function(){
		var _this = this;
		_this.interval = setInterval(function(){
			//if(_this.speed <= 10){
			_this.speed += 0.25;
			//}
			/**/
		},1000);
		_this.interval2 =setInterval(function(){
			_this.genorateFood();
		},400);
	},
	reset : function(){
		this.count = 0;
		this.foodList = [];
		this.bgloop = 0;
		this.score = 0;
		this.timmer = null;
		this.time = 0;
		this.speed = 10;
		this.intervalFlag = true;
		$('#gamepanel').html('<div class="score-wrap"><span id="score">0分</span><div class="heart" data-num="5"><span id="heart1"></span><span id="heart2"></span><span id="heart3"></span><span id="heart4"></span><span id="heart5"></span></div></div><canvas id="stage" width="'+$(window).width()+'" height="'+$(window).height()+'"></canvas>');
		$('#score').text(this.score);
		//this.init();
	},
	updateShare:function(){
		var score = this.score;
		$('#sscore').html(score*10);
		shareData.title =  '我收集了'+score*10+'个地中海美食，你敢来挑战吗？';//'我一共收集了'+score+'个地中海美食，你敢来挑战么？';
		$('#__newsapp_sharetext').html('我收集了'+score*10+'个地中海美食，你敢来挑战吗？');
		$('#__newsapp_sharewxtitle').html('我收集了'+score*10+'个地中海美食，你敢来挑战吗？');
	},
	isMobile : function(){
		var sUserAgent= navigator.userAgent.toLowerCase(),
		bIsIpad= sUserAgent.match(/ipad/i) == "ipad",
		bIsIphoneOs= sUserAgent.match(/iphone os/i) == "iphone os",
		bIsMidp= sUserAgent.match(/midp/i) == "midp",
		bIsUc7= sUserAgent.match(/rv:1.2.3.4/i) == "rv:1.2.3.4",
		bIsUc= sUserAgent.match(/ucweb/i) == "ucweb",
		bIsAndroid= sUserAgent.match(/android/i) == "android",
		bIsCE= sUserAgent.match(/windows ce/i) == "windows ce",
		bIsWM= sUserAgent.match(/windows mobile/i) == "windows mobile",
		bIsWebview = sUserAgent.match(/webview/i) == "webview";
		return (bIsIpad || bIsIphoneOs || bIsMidp || bIsUc7 || bIsUc || bIsAndroid || bIsCE || bIsWM);
     }
}