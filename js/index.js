//音乐开关
function controlMus(isClose){
	var obj = document.getElementById("musicWrap").getElementsByTagName("a")[0],
		thisStatus = obj.getAttribute("data-status");
	if(thisStatus == "off" && isClose !=1){
		document.getElementById("controlText").innerHTML = "打开";
		document.getElementById("controlText").style.display = "block";
		document.getElementById("media").play();
		obj.setAttribute("class","btn_music on");
		obj.setAttribute("data-status","on");
	}else{
		document.getElementById("controlText").innerHTML = "关闭";
		document.getElementById("controlText").style.display = "block";
		document.getElementById("media").pause();
		obj.setAttribute("class","btn_music");
		obj.setAttribute("data-status","off");
	}
	if( isClose == 1){
		document.getElementById("musicWrap").style.display = "none";
	}
	setTimeout(function(){
		document.getElementById("controlText").style.display = "none";
	},1000);
}
function isWX(){
	if(window.navigator.userAgent.toLowerCase().match(/MicroMessenger/i) == 'micromessenger'){
		return true;
	}else{
		return false;
	}
}
function isWY(){
	if(window.navigator.userAgent.search(/NewsApp/)>=0){
		return true;
	}else{
		return false;
	}
}
//var domain = 'http://p5.yokacdn.com/pic/shzt/swatch/15summer/mobile/';
var domain = '';
var imgArray = [domain + 'images/tp.png',domain+'images/fp.png',domain + 'images/op.png', domain+'images/cp.png'];
var stage2c = 'on';
if(!isWX()){
	$('#stage1').hide().remove();
	stage2c = 'first';
}
$("body").append('<style>'+
	'#stage2.'+stage2c+' img.s2-m1 { opacity: 1; -webkit-transform: translateY(0); -webkit-transition: all 0.5s 0.2s; }'+
	'#stage2.'+stage2c+' img.s2-m2 { opacity: 1; -webkit-transform: translateX(0); -webkit-transition: all 0.5s 0.6s; }'+
	'#stage2.'+stage2c+' img.s2-m3 { opacity: 1; -webkit-transform: translateX(0); -webkit-transition: all 0.5s 1s; }'+
	'#stage2.'+stage2c+' img.s2-m4 {opacity: 1; -webkit-transform: translateY(0); -webkit-transition: all 0.5s 1.4s; }'+
	'#stage2.'+stage2c+' .s2-con { opacity: 1; -webkit-transform: translateY(0); -webkit-transition: all 0.5s 1.6s; }</style>');

$('#gamepanel').append('<canvas id="stage" width="'+$(window).width()+'" height="'+$(window).height()+'"></canvas>');
//加载成功
/*	window.onload = */function init(){

	$('.loading').addClass('out');
	//初始加载分屏动画
	mySwiper = $("#mySwiper").swiper({
		mode:"vertical",
		loop: false,
		noSwiping:true,
		noSwipingNext:true,
		noSwipingPrev:true,
		queueEndCallbacks:true,
		moveStartThreshold:10,
		slideActiveClass: 'on',
		paginationClickable: true,
		onInit:function(){
			controlMus(0);
			if($(mySwiper.slides[mySwiper.activeIndex]).attr('id') == 'stage1'){
				mzVirtualPV('#第一页', '抽奖分享');//add 第一页
			}else{
				mzVirtualPV('#游戏规则页', '抽奖分享');//add 游戏规则页
			}

		},
		onSlideChangeEnd : function(){
			if(mySwiper.activeIndex == mySwiper.slides.length - 1){
				$('.arrow').hide();
			}else{
				$('.arrow').show();
			}
			if($(mySwiper.slides[mySwiper.activeIndex]).attr('id') == 'stage1'){
				mzVirtualPV('#第一页', '抽奖分享');//add 第一页
			}else if($(mySwiper.slides[mySwiper.activeIndex]).attr('id') == 'stage2'){
				mzVirtualPV('#游戏规则页', '抽奖分享');//add 游戏规则页
			}else{
				mzVirtualPV('#选择食材页', '抽奖分享');//add 选择食材页
			}

		}
	});

	//出结果
	$('#stage3 a').on('click',function(e){
		e.preventDefault();
		mzVirtualPV('#开始游戏页', '抽奖分享');//add 开始游戏页
		$('.arrow').hide();
		$("#stage4 .s4-s3-item").css("display","none");
		var itemNum = $(this).index();
		//controlMus(1); //关闭并隐藏音乐
		$('#mySwiper').hide();//add
		$($("#stage4 .s4-s3-item")[itemNum]).show();
		$("#stage4").show();
		$('#mySwiper').hide();
		$('#container').show();//add
		gameMonitor.init(itemNum,imgArray[itemNum]);
	});
	//开始游戏

	//提交结果
	$('#submitRes').on('touchstart',function(){
		$(this).hide();
		$('#tip').show();
		var name = $('#username').val(),
			phone = $('#mobile').val();

		var	a	=	[];
		a.push({name:"name",value:name});	//姓名
		a.push({name:"mobile",value:phone});	//手机
		a.push({name:"score",value:gameMonitor.score*10}); //用户得分
		$.getJSON("http://zhuanti.yoka.com/swatch/15summer/update.php?callback=?",a,function(data){
			if(data.status == "1") {
				mzBtnTrack('完成提交',1);
				mzVirtualPV('#分享提示页', '抽奖分享');//add 分享提示页
				if(isWY()){
					$('.wyShareDiv').addClass('on');
				}else{
					$('.shareDiv').addClass('on');
				}

				/*else if(isWX()){
				 $('.shareDiv').addClass('on');
				 }else{
				 document.title = shareData.title;
				 $('#stage4').hide();
				 $('#resultPanel').hide();
				 $('#container').hide();
				 $('#stage11').show();
				 getRank();
				 }*/
				shareFlag = true;
			}else{
				alert(data.msg);
			}
			$('#submitRes').show();
			$('#tip').hide();
		});

		/*if(!(name!=""&&(/^[ ]+$/).test(name))){
		 alert('请输入姓名');
		 }
		 if(!(phone!=""&&(/^[ ]+$/).test(phone)&&!(/^1[3|5][0-9]\d{4,8}$/.test(phone)))){
		 alert('手机号输入有误');
		 }*/

	});

	//查看我的排名
	$('#viewMe').on('touchstart',function(){
		mzBtnTrack('查看我的排名',1);
		mzVirtualPV('#我的排名页', '抽奖分享');//add 我的排名页
		$('#stage11').hide();
		$('#stage12').show();
	});
	$('#playAgain').on('click', function(e){
		e.preventDefault();
		mzBtnTrack('再玩一次',1);
		mzVirtualPV('#选择食材页', '抽奖分享');//add 选择食材页
		$('#resultPanel').hide();
		$('.shareDiv').removeClass('on');
		$('.wyShareDiv').removeClass('on');
		$('#stage12').hide();
		$('#stage3').addClass('swiper-no-swiping');
		$('#mySwiper').show();
		$('#container').hide();
		gameMonitor.reset();
	});
	$('#musicWrap').removeClass('f-hide');
	$('#musicWrap').on('touchstart',function(e){
		e.preventDefault();
		controlMus(0);
	})
	$('.loading').on('webkitTransitionEnd',function(){
		setTimeout(function(){
			if(!isWX()){
				$('#stage2').addClass('first');
			}
			$('.arrow').show();
		},100);
		$('.loading').off('webkitTransitionEnd');
	})
}
$(function(){
	var _srcList = [],
		i = 0;　　　　　　　　 //获取所有图片路径，存为数组
	$('img').each(function(){
		_srcList.push($(this).attr('src'));
	})
	function imgLoadComplate(imgSrc){
		var _img = new Image();
		_img.src = imgSrc;
		_img.onload = function(){　　　　　　　　　　　　　　　　 //判断是否加载到最后一个图片
			if (i < _srcList.length-1) {
				i++;
				imgLoadComplate(_srcList[i]);
			}else{
				init();
			}
		}
	}
	imgLoadComplate(_srcList[i]);
})
$('document').on('touchstart',function(e){
	e.preventDefault();
})